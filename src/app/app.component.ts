import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { concatMap, of } from 'rxjs';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  private readonly apiUrl = 'http://localhost:3000/notes';

  public dataSource = [] as any;

  displayedColumns: string[] = ['id', 'title', 'desc', 'time', 'btn'];
  title = 'notes-fe';

  constructor(private http: HttpClient, public dialog: MatDialog) {}

  ngOnInit(): void {
    this.http
      .get(this.apiUrl)
      .subscribe((res: any) => (this.dataSource = res.data));
  }

  deleteNote(id: number) {
    return confirm('Confirm delete')
      ? of(null)
          .pipe(
            concatMap(() => this.http.delete(this.apiUrl + `/${id}`)),
            concatMap((res) => this.http.get(this.apiUrl))
          )
          .subscribe((res: any) => (this.dataSource = res.data))
      : null;
  }

  openDialog(action: string, note?: any): void {
    if (action === 'create') {
      const dialogRef = this.dialog.open(DialogComponent, {
        width: '400px',
        data: { action, note },
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (result?.title && result?.desc) {
          of(null)
            .pipe(
              concatMap(() =>
                this.http.post(this.apiUrl, {
                  title: result?.title,
                  describe: result.desc,
                })
              ),
              concatMap(() => this.http.get(this.apiUrl))
            )
            .subscribe((resData: any) => {
              alert('Create note successfully');
              return (this.dataSource = resData.data);
            });
        } else alert('title and description are empty!');
      });
    } else {
      const dialogRef = this.dialog.open(DialogComponent, {
        width: '400px',
        data: { action, note },
      });

      dialogRef.afterClosed().subscribe((result) => {
        if (result?.title && result?.desc) {
          of(null)
            .pipe(
              concatMap(() =>
                this.http.put(this.apiUrl + `/${note.id}`, {
                  title: result?.title,
                  describe: result.desc,
                })
              ),
              concatMap(() => this.http.get(this.apiUrl))
            )
            .subscribe((resData: any) => {
              alert('Update note successfully');
              return (this.dataSource = resData.data);
            });
        } else alert('title and description are empty!');
      });
    }
  }
}
